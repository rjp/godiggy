package main

import (
	"encoding/json"
	"fmt"
    "log"
	"io/ioutil"
	"net/url"
	"sort"
	"strconv"
    "database/sql"

	"github.com/rjp/anaconda"
	_ "github.com/mattn/go-sqlite3"
	"github.com/azer/crud"
)

type consumerAuth struct {
	ConsumerKey    string `json:"consumer_key"`
	ConsumerSecret string `json:"consumer_secret"`
}

type clientAuth struct {
	Token  string
	Secret string
}

type diggyMoo struct {
	consumer consumerAuth
	client   clientAuth
	db       *crud.DB
}

func (d *diggyMoo) maxIDSent() (int, error) {
	maxID := 0

	err := d.db.Read(&maxID, "SELECT max(id) FROM sent")

	return maxID, err
}

func (d *diggyMoo) maxIDSeen() (int, error) {
	maxID := 0

	err := d.db.Read(&maxID, "SELECT max(id) FROM tweet_blob")

	return maxID, err
}

func (d *diggyMoo) readAuthJSON() {
	jsonFile, e := ioutil.ReadFile(myOptions.consumer)
	if e != nil {
		panic(e)
	}

	e = json.Unmarshal(jsonFile, &d.consumer)
	if e != nil {
		panic(e)
	}

	jsonFile, e = ioutil.ReadFile(myOptions.client)
	if e != nil {
		panic(e)
	}

	e = json.Unmarshal(jsonFile, &d.client)
	if e != nil {
		panic(e)
	}
}

func newDiggyMoo() diggyMoo {
	d := diggyMoo{}

	// Open the database
	db, err := crud.Connect("sqlite3", myOptions.database)
	if err != nil {
		panic(err)
	}

    /*
	err = db.CreateTables(tweetBlob{}, sent{}, mediaBlob{})
	if err != nil {
		panic(err)
	}
    */

	d.db = db
	// Read the configurations
	d.readAuthJSON()

	return d
}

// {[] [{[95 118] https://t.co/aHl0OSonS0 soundcloud.com/richconaty/so-… https://soundcloud.com/richconaty/so-ashamed}] {[]} [] [{685129794418257920 685129794418257920 http://pbs.twimg.com/media/CYIR6wVWMAAmVI8.jpg https://pbs.twimg.com/media/CYIR6wVWMAAmVI8.jpg https://t.co/QozCOXV04b pic.twitter.com/QozCOXV04b http://twitter.com/RichConaty/status/685129796024700928/photo/1 {{170 132 fit} {150 132 crop} {170 132 fit} {170 132 fit}} 0  photo [119 142] {[] 0 []}}]}

func joinRunes(a []rune, b string, c []rune) []rune {
	z := []rune{}
	z = append(z, a...)
	z = append(z, []rune(b)...)
	z = append(z, c...)
	return z
}

type replacer struct {
	start int
	end   int
	text  string
    kind  string
}
type replacements []replacer

func (slice replacements) Len() int { return len(slice) }

// Backwards because we want a descending sort
func (slice replacements) Less(i, j int) bool {
	return slice[i].start > slice[j].start
}
func (slice replacements) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func fixUpEntities(tweet anaconda.Tweet) string {

    // We need to know what text we're going to display
    visible := tweet.DisplayTextRange

    textStart := visible[0]
    textEnd := visible[1]
    log.Printf("s=%d e=%d l=%d\n[[%s]]\n", textStart, textEnd, len(tweet.FullText), tweet.FullText)

	// We have to replace the entities all in one pass
	// running from the deepest to the shallowest - else
	// we'll be changing the indices as we go and making
	// an unholy mess.  Hence we accumulate all the bits
	// of replacement text first.
	replacementList := replacements{}

	for _, user := range tweet.Entities.User_mentions {
		start, end := user.Indices[0], user.Indices[1]
		username := user.Screen_name
		replace := fmt.Sprintf("<a href='https://twitter.com/%s'>@%s</a>", username, username)
		replacementList = append(replacementList, replacer{start, end, replace, "user"})
	}

	for _, hashtag := range tweet.Entities.Hashtags {
		start, end := hashtag.Indices[0], hashtag.Indices[1]
		hashtagText := hashtag.Text
		replace := fmt.Sprintf("<a href='https://twitter.com/hashtag/%s'>#%s</a>", hashtagText, hashtagText)
		replacementList = append(replacementList, replacer{start, end, replace, "hashtag"})
	}

	for _, url := range tweet.Entities.Urls {
		expanded := url.Expanded_url
		display := url.Display_url
		start, end := url.Indices[0], url.Indices[1]
		replace := fmt.Sprintf("<a href='%s'>%s</a>", expanded, display)
		replacementList = append(replacementList, replacer{start, end, replace, "url"})
	}

	// Remove the links to media since we embed them later
	for _, media := range tweet.Entities.Media {
		start, end := media.Indices[0], media.Indices[1]
		replacementList = append(replacementList, replacer{start, end, "", "media"})
	}

    // Create an empty list the same type as `replacementList`
    log.Println("164")
    tmpR := replacementList[0:0]

    // Any entities which are outside `DisplayTextRange` get ignored.
    for _, r := range replacementList {
        //
        if r.start < textEnd && r.end > textStart {
            // Because we're chopping a chunk out of the text due to
            // `DisplayTextRange`, we need to adjust our entity positions
            // because they're not adjusted for the new text range.
            r.start = r.start - textStart
            r.end = r.end - textStart
            //
            tmpR = append(tmpR, r)
        } else {
                log.Printf("!! ignoring %s because [%d:%d] outside [%d:%d]\n", r.kind, r.start, r.end, textStart, textEnd)
        }
    }

        log.Println("PRE ", replacementList)
    replacementList = tmpR
	sort.Sort(replacementList)
		log.Println("POST", replacementList)

    fullText := []rune(tweet.FullText)
    text := fullText[textStart:textEnd]
    log.Printf("191 l=%d s=%d e=%d [%s]\n", len(tweet.FullText), textStart, textEnd, string(text))

        log.Printf("DTR=[%d:%d] [%s]\n", textStart, textEnd, string(text))

	for _, r := range replacementList {
            log.Printf("REP [0:%d] [%s] [%d:]\n", r.start, r.text, r.end)
        if r.end < textEnd {
            log.Printf("203 [%s] l=%d s=%d e=%d\n", string(text), len(text), r.start, r.end)
            text = joinRunes(text[0:r.start], r.text, text[r.end:])
        } else {
            log.Printf("206 l=%d s=%d e=%d\n", len(text), r.start, r.end)
            log.Printf("207 [%s] [%s]\n", text[0:r.start], r.text)
            text = joinRunes(text[0:r.start], r.text, []rune(""))
        }
	}
		log.Printf("DBG> %d\n%s\n", tweet.Id, string(text))

	return string(text)
}

func (d diggyMoo) haveSeenTweet(tweetID string) bool {
	return false
}

func (d diggyMoo) insertTweets(tweets []anaconda.Tweet, fromTimeline bool) {
	tx, err := d.db.Begin()
	if err != nil {
		panic(err)
	}

	// Insert a bunch of tweets here
	for _, tweet := range tweets {
		// WHY THE BUGGERING HELL IS THIS MULTIVALUED?
		twt, _ := tweet.CreatedAtTime()

		// Expand the entities to make the output useful
		newText := fixUpEntities(tweet)

        var RetweetedID sql.NullInt64
        RetweetedID.Valid = false
        if tweet.RetweetedStatus != nil {
            RetweetedID.Int64 = tweet.RetweetedStatus.Id
            RetweetedID.Valid = true
        }

        var QuotedID sql.NullInt64
        QuotedID.Valid = false
        if tweet.QuotedStatus != nil {
            QuotedID.Int64 = tweet.QuotedStatus.Id
            QuotedID.Valid = true
        }

        // TODO Store the raw* tweet data somewhere as a backup?
            log.Printf("%d r=%t q=%t t=%t %s\n", tweet.Id, RetweetedID.Valid, QuotedID.Valid, tweet.Truncated, tweet.Text)
            log.Println("")

        // Remember which tweets came from our timeline and which ones came from
        // quotes or retweets - because we don't want to display those as part of
        // our generic timeline.
        var Secondary sql.NullInt64
        Secondary.Int64 = 1
        Secondary.Valid = fromTimeline

        // TODO Update the schema to record whether something was re
		err = tx.Create(&tweetBlob{
			int(tweet.Id), twt, tweet.Favorited,
			tweet.User.ScreenName, tweet.User.Name,
			tweet.InReplyToScreenName,
			int(tweet.InReplyToStatusID),
			newText,
			tweet.Source,
			tweet.User.ProfileImageURL,
            RetweetedID,
            QuotedID,
            Secondary,
		})
		if err != nil {
			panic(err)
		}

		// Collect the media entities into a table
		for _, media := range tweet.Entities.Media {
				log.Println("MD", tweet.Id, media.Sizes.Thumb)
				log.Println("MD", tweet.Id, media.Media_url)
				log.Println("MD", tweet.Id, media.Display_url)
			err = tx.Create(&mediaBlob{int(tweet.Id), media.Media_url})
			if err != nil {
				panic(err)
			}
		}

	}

	err = tx.Commit()
	if err != nil {
		panic(err)
	}
}

func main() {
	// First, do no harm. Second, parse command line arguments.
	doFlags("fetcher")

	diggymoo := newDiggyMoo()

	anaconda.SetConsumerKey(diggymoo.consumer.ConsumerKey)
	anaconda.SetConsumerSecret(diggymoo.consumer.ConsumerSecret)
	api := anaconda.NewTwitterApi(diggymoo.client.Token, diggymoo.client.Secret)

	// We want ONE HUNDRED tweets.
	// TODO make this option-settable later
	v := url.Values{}
	v.Set("count", "100")
    v.Set("tweet_mode", "extended")

	if myOptions.sinceFetch {
		since, err := diggymoo.maxIDSeen()
		if err == nil {
			lastSeen := strconv.Itoa(since)
			v.Set("since_id", lastSeen)
			if myOptions.verbose {
				fmt.Printf("fetching `since_id > %s`\n", lastSeen)
			}
		} else {
			fmt.Println("Error fetching `maxIDSeen`:", err)
		}
	}

	if myOptions.sinceSend {
		since, err := diggymoo.maxIDSent()
		if err == nil {
			v.Set("since_id", strconv.Itoa(since))
		}
	}
// If we're not debugging, trash all the log output
    if ! myOptions.debug {
        log.SetFlags(0)
        log.SetOutput(ioutil.Discard)
    }

		log.Println(v)
	timeline, err := api.GetHomeTimeline(v)
	if err != nil {
		panic(err)
	}

    secondaryTweets := []anaconda.Tweet{}
    for _, tweet := range timeline {
        if tweet.RetweetedStatus != nil {
            secondaryTweets = append(secondaryTweets, *tweet.RetweetedStatus)
        }
        if tweet.QuotedStatus != nil {
            secondaryTweets = append(secondaryTweets, *tweet.QuotedStatus)
        }
    }
    //fmt.Printf("%#v\n", secondaryTweets)

	// INSERT AND UPDATE
	diggymoo.insertTweets(timeline, false)
	diggymoo.insertTweets(secondaryTweets, true)

	if myOptions.verbose {
		for _, tweet := range timeline {
			fmt.Printf("%-16d %s\n", tweet.Id, tweet.User.ScreenName)
			fmt.Println(tweet.Text)
			fmt.Println("------")
		}

		for _, tweet := range secondaryTweets {
			fmt.Printf(">> %-16d %s\n", tweet.Id, tweet.User.ScreenName)
			fmt.Printf(">> %s\n", tweet.Text)
			fmt.Println(">> ------")
		}
	}

}
