# golang version of diggymoo

## Building

You'll need

1. [Go](https://golang.org/)
1. [redo](https://github.com/apenwarr/redo/) (or use the included `do` shellscript)

Either call `redo` or `./do` and it should build the three apps.

## Configuring

First you need [Twitter application keys](https://apps.twitter.com/) in `consumer.json`.

    {
     "consumer_key": "CONSUMERKEY",
     "consumer_secret": "CONSUMERSECRET"
    }

Then you need Twitter client keys in `client.json` - you can generate these using OAuth
(not recommended because faff) or just use your own from the Twitter app keys page.

    {
        "token": "BING-BONGTOKEN",
        "secret": "SECRET"
    }

## Running

`dbhandler` handles database creation and migrations - it is designed to run before
`fetcher` or `sender` but doesn't necessarily have to be every single time.

My personal fetch script looks like this:

    cd /place/for/godiggy
    $BIN/dbhandler && $BIN/fetcher

## Options

### Fetcher

Yeah, I've no idea why there's `-sinceFetch` and also `-since_fetch`.
Also why `-debug` mentions the sent table.

      -client string
                Client keys (default "client.json")
      -consumer string
                Consumer keys (default "consumer.json")
      -database string
            SQLite3 database file (created if absent) (default "diggymoo.db")
      -debug
            Don't update the sent table
      -sinceFetch
            Only fetch tweets since our last fetch
      -since_fetch
            Only fetch tweets since our last fetch
      -verbose
            Output tweets as they're fetched

### Sender specific

      -database string
            SQLite3 database file (created if absent) (default "diggymoo.db")
      -debug
            Don't update the sent table
      -email string
            Email address to send to (default "none@none.none")
      -quoted
            Exclude quoted retweets
      -retweets
            Exclude plain retweets
      -since_send
            Only send tweets since our last email
      -template string
            Output template (golang) (default "email.goml")
      -tweets
            Exclude original tweets

## Other information

[[golang's template language](https://golang.org/pkg/text/template/)]

## Available data in the template

    .scope         List of tweets (see below for tweet data)
    .posts_length  How many tweets we have
    .email         Value of the `-email` option

Within `.scope`, we have

    .Original      The original tweet
    .Retweeted     The retweeted tweet if it's a retweet
    .Quoted        The quoted tweet if it's a quoted retweet

### Tweet blob

    .Id         Tweet ID
    .Wh         `time.Time` representing creation time
    .Fav        Whether this tweet is favourited
    .S_from     Screen name of sender
    .N_from     Real name of sender
    .S_to       Screen name of recipient if it's a reply
    .I_to       Tweet ID of the parent if it's a reply
    .Body       Content of the tweet
    .Source     HTML representing the source client (ie Tweetbot)
    .Avatar     URL to the user's avatar
    .RawSource  `template.HTML` version of `.Source` (will not be HTML escaped)
    .Images     The images attached to this tweet

## Other information

This gives the percentages of each type of tweet grouped by day.

    select day, tweets, 100*original/tweets "original %", 100*replies/tweets "reply %", 100*retweets/tweets "retweet %", 100*quoted/tweets "quoted %", zz "unique IDs" from (select qq day, t tweets, t-re-rt-qt original, re replies, rt retweets, qt quoted, zz from (select count(1) t, count(nullif(i_to,0)) re, count(retweeted) rt, count(quoted) qt, strftime('%Y-%m-%d', wh) qq, count(distinct s_from) zz from tweet_blob where secondary is null group by qq));

Which gives something like this (with `.header on; .mode column`)

    day         tweets      original %  reply %     retweet %   quoted %    unique IDs
    ----------  ----------  ----------  ----------  ----------  ----------  ----------
    2017-09-15  157         29          15          33          21          46
    2017-09-16  666         39          20          26          14          95
    2017-09-17  630         30          14          36          18          90
    2017-09-18  539         32          11          40          16          104
    2017-09-19  211         26          7           49          17          42
