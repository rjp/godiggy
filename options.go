package main

import (
	"flag"
	"fmt"
)

// buildvars
var (
	commit  string
	builtAt string
	builtBy string
	builtOn string
	version string
)

var myOptions struct {
	consumer   string
	client     string
	template   string
	database   string
	email      string
	debug      bool
	verbose    bool
	sinceFetch bool
	sinceSend  bool
    excludeTweets bool
    excludeRetweets bool
    excludeQuoted bool
    doMigrations bool
    doReset bool
    resetConfirm string
}

func doFlags(iAm string) {
	flag.StringVar(&myOptions.database, "database", "diggymoo.db", "SQLite3 database file (created if absent)")
    flag.BoolVar(&myOptions.debug, "debug", false, "Don't update the sent table")

    if iAm == "fetcher" {
        flag.StringVar(&myOptions.consumer, "consumer", "consumer.json", "Consumer keys")
        flag.StringVar(&myOptions.client, "client", "client.json", "Client keys")
        flag.BoolVar(&myOptions.verbose, "verbose", false, "Output tweets as they're fetched")
        flag.BoolVar(&myOptions.sinceFetch, "since_fetch", false, "Only fetch tweets since our last fetch")
        flag.BoolVar(&myOptions.sinceFetch, "sinceFetch", false, "Only fetch tweets since our last fetch")
    }

    if iAm == "sender" {
        flag.StringVar(&myOptions.email, "email", "none@none.none", "Email address to send to")
        flag.StringVar(&myOptions.template, "template", "email.goml", "Output template (golang)")
        flag.BoolVar(&myOptions.sinceSend, "since_send", false, "Only send tweets since our last email")
// We're doing this my way and I gladly accept my role as human colander in the revolution.
        flag.BoolVar(&myOptions.excludeTweets, "tweets", false, "Exclude original tweets")
        flag.BoolVar(&myOptions.excludeRetweets, "retweets", false, "Exclude plain retweets")
        flag.BoolVar(&myOptions.excludeQuoted, "quoted", false, "Exclude quoted retweets")
    }

    if iAm == "dbhandler" {
        flag.BoolVar(&myOptions.doMigrations, "migrate", false, "Migrate database schema if necessary")
        flag.BoolVar(&myOptions.doReset, "reset", false, "Reset database schema")
        flag.StringVar(&myOptions.resetConfirm, "confirm", "", "Confirm reset of database schema (must be 'yes')")
    }

	version = fmt.Sprintf("r%s built %s by %s (%s)", commit, builtAt, builtBy, builtOn)

	flag.Usage = func() {
		fmt.Println(version)
		fmt.Println("")
		flag.PrintDefaults()
	}

	flag.Parse()
	if myOptions.debug && iAm != "sender" {
		fmt.Printf("%#v\n", myOptions)
	}

}
