. ./builder.sh
if [ -f $2.od ]; then
    . ./$2.od
fi
redo-ifchange $DEPS
GOBIN=${GOBIN:-go}
$GOBIN build -ldflags "-s -w -X main.commit=${commit} -X main.builtAt='${built_at}' -X main.builtBy=${built_by} -X main.builtOn=${built_on}" -o $3 $DEPS
