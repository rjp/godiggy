package main

import (
    "fmt"
    "database/sql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/rjp/crud"
)

func main() {
	// First, do no harm. Second, parse command line arguments.
	doFlags("dbhandler")

	// Open the database for our search query.
	db, err := crud.Connect("sqlite3", myOptions.database)
	if err != nil {
		panic(err)
	}

    if myOptions.doReset {
        if myOptions.resetConfirm == "yes" {
            err = db.ResetTables(tweetBlob{}, sent{}, mediaBlob{})
            if err != nil { panic(err) }
        }
    }

    err = db.CreateTables(tweetBlob{}, sent{}, mediaBlob{})
    if err != nil { panic(err) }

    // At this point, we either have 
    // 1. Empty blank tables from a fresh database
    // 2. A mix of old and new tables (if we've added some)
    // 3. All old tables

    // Add the `retweeted` column to `tweet_blob`
    migrationAddRetweetedField(db)
    migrationAddQuotedField(db)
    migrationAddSecondaryField(db)

}

func migrationAddRetweetedField(db *crud.DB) {
    t := 0
    err := db.Read(&t, "SELECT id FROM tweet_blob WHERE retweeted IS NOT NULL LIMIT 0")
    if err == sql.ErrNoRows {
        return
    }
    result, err := db.Exec("ALTER TABLE tweet_blob ADD retweeted int default null")
    if err != nil { panic(err) }
    fmt.Println("Added retweeted", result)
}

func migrationAddQuotedField(db *crud.DB) {
    t := 0
    err := db.Read(&t, "SELECT id FROM tweet_blob WHERE quoted IS NOT NULL LIMIT 0")
    if err == sql.ErrNoRows {
        return
    }
    result, err := db.Exec("ALTER TABLE tweet_blob ADD quoted int default null")
    if err != nil { panic(err) }
    fmt.Println("Added quoted", result)
}

func migrationAddSecondaryField(db *crud.DB) {
    t := 0
    err := db.Read(&t, "SELECT id FROM tweet_blob WHERE secondary IS NOT NULL LIMIT 0")
    if err == sql.ErrNoRows {
        return
    }
    result, err := db.Exec("ALTER TABLE tweet_blob ADD secondary int default null")
    if err != nil { panic(err) }
    fmt.Println("Added secondary", result)
}
