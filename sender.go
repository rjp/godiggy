package main

import (
	"bytes"
    "database/sql"
	"fmt"
    "io/ioutil"
    "log"
	"html/template"
	"time"
    "strings"

	_ "github.com/mattn/go-sqlite3"
	"github.com/rjp/crud"
)

// `crud` only works with special queries if they begin with `SELECT`
//
// Does this really need to be a `JOIN`? Why not just store the "I have sent this" in the tweet table?
// In a multi-user environment, this would make sense - everyone would have their own copy* of the
// `sent` table referencing a single shared tweet table. But in a single-user setup, there's no real
// need -and- it prevents us from leveraging `crud` as effectively as we might.
const sqlFindTweetsAndRetweets = "SELECT tweet_blob.* from tweet_blob left join sent using (id) where sent.id is null and secondary is null"

type bareTweet struct {
	tweetBlob
	Images []*mediaBlob
	// Not a `string` because we don't want it escaping in the output
	RawSource template.HTML
	RawBody   template.HTML
    Retweeted tweetBlob
    Quoted tweetBlob
    IsRetweet bool
    IsQuote bool
}

type tweetData struct {
    Original bareTweet
    Retweeted bareTweet
    Quoted bareTweet
}

func indent(i string, b string) string {
    if i == "" {
        i = "  >> "
    }
    lines := strings.Split(b, "\n")
    out := []string{}
    for _, l := range lines {
        out = append(out, "  >> " + l)
    }
    return strings.Join(out, "\n")
}

func noEscape(b string) template.HTML {
    return template.HTML(b)
}

func main() {
	doFlags("sender")

// If we're not debugging, trash all the log output
    if ! myOptions.debug {
        log.SetFlags(0)
        log.SetOutput(ioutil.Discard)
    }

	// Open the database for our search query.
	db, err := crud.Connect("sqlite3", myOptions.database)
	if err != nil {
		panic(err)
	}

	// We need a second database handle because we want to run
	// an inserting transaction at the same time we're returning
	// rows from a query.
	dbW, err := crud.Connect("sqlite3", myOptions.database)
	if err != nil {
		panic(err)
	}

	tweets := []*tweetBlob{}

    // Default to "original and retweets" unless otherwise told.
    query := sqlFindTweetsAndRetweets

    // We can directly append our extra query bits here because we already
    // know we have a trailing `where` clause on the original.
    if myOptions.excludeRetweets {
        query = query + " and tweet_blob.retweeted is null"
    }

    if myOptions.excludeQuoted {
        query = query + " and tweet_blob.quoted is null"
    }

    if myOptions.excludeTweets {
        query = query + " and (tweet_blob.quoted is not null or tweet_blob.retweeted is not null)"
    }

    err = db.Read(&tweets, query)

	if err != nil {
		panic(err)
	}

	type HamlVars map[string]interface{}

	// Can we start a transaction whilst we have a query returning?
	tx, err := dbW.Begin()
	if err != nil {
		panic(err)
	}

	now := time.Now()

	scope := []tweetData{}
	for _, tweetrow := range tweets {
        // These are ok to be blank because we guard against using them in the template
        // with the `IsRetweeted` and `IsQuoted` booleans.
        retweeted := tweetBlob{}
        quoted := tweetBlob{}

		media := []*mediaBlob{}
        rtMedia := []*mediaBlob{}
        qtMedia := []*mediaBlob{}

		err = db.Read(&media, "WHERE id=?", tweetrow.ID)
		if err != nil {
			panic(err)
		}

        if tweetrow.Quoted.Valid {
            err = db.Read(&quoted, "WHERE id=?", tweetrow.Quoted.Int64)
            if err != nil && err != sql.ErrNoRows { panic(err) }

            err = db.Read(&qtMedia, "WHERE id=?", quoted.ID)
            if err != nil {
                panic(err)
            }
        }

        if tweetrow.Retweeted.Valid {
            err = db.Read(&retweeted, "WHERE id=?", tweetrow.Retweeted.Int64)
            if err != nil && err != sql.ErrNoRows { panic(err) }

            err = db.Read(&rtMedia, "WHERE id=?", retweeted.ID)
            if err != nil {
                panic(err)
            }
        }

		bt := bareTweet{*tweetrow, media, template.HTML(tweetrow.Source), template.HTML(tweetrow.Body),
            retweeted, quoted, tweetrow.Retweeted.Valid, tweetrow.Quoted.Valid}
        rt := bareTweet{}
        if tweetrow.Retweeted.Valid {
            rt = bareTweet{retweeted, rtMedia, template.HTML(retweeted.Source), template.HTML(retweeted.Body),
                tweetBlob{}, tweetBlob{}, false, false}
        }
        qt := bareTweet{}
        if tweetrow.Quoted.Valid {
            qt = bareTweet{quoted, qtMedia, template.HTML(quoted.Source), template.HTML(quoted.Body),
                tweetBlob{}, tweetBlob{}, false, false}
        }
        td := tweetData{bt, rt, qt}
		scope = append(scope, td)

		err = tx.Create(&sent{tweetrow.ID, now})
		if err != nil {
			panic(err)
		}
	}

	allData := HamlVars{
		"boundary":     "F14NG35",
		"scope":        scope,
		"posts_length": len(scope),
		// TODO send email from within this script?
		"email": myOptions.email,
	}

    // This is only used in the `cli.goml` template for now.

    funcMap := template.FuncMap{"indent": indent, "noescape": noEscape}
    log.Printf("%#v\n", funcMap)
    templateFiles := []string{}

    switch myOptions.template {
    case "text":
        templateFiles = []string{"cli.goml"}
    case "email":
        templateFiles = []string{"email.goml", "html.goml"}
    case "html":
        templateFiles = []string{"html.goml"}
    default:
        // Assume that an arbitrary template is going to be HTML
        templateFiles = []string{myOptions.template, "html.goml"}
    }

    log.Printf("TF=%#v\n", templateFiles)
    t := template.Must( template.New(templateFiles[0]).Funcs(funcMap).ParseFiles(templateFiles...) )

	var doc bytes.Buffer
	err = t.Execute(&doc, allData)
	if err != nil {
		panic(err)
	}

	// We have no way of knowing if this output will get sent but
	// not committing the transaction if it fails might help.
	_, err = fmt.Println(doc.String())
	if err != nil {
		panic(err)
	}

	if myOptions.debug {
		// This is a bit lazy but it's easier than a big `if`
		err = tx.Rollback()
		if err != nil {
			panic(err)
		}
	} else {
		// We can probably commit now.
		err = tx.Commit()
		if err != nil {
			panic(err)
		}
	}
}
